﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {

    public GameObject destinationTeleport;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        other.gameObject.transform.parent.transform.position = destinationTeleport.transform.position;
    }
}
