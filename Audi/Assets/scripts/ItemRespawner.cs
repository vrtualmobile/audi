﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemRespawner : MonoBehaviour {

	public GameObject mark;
	public GameObject spray;
	public GameObject upgrade;
	// Use this for initialization
	private List<GameObject> items = new List<GameObject>();
	private RespawnObj myGridRespawner;
	private float time =0;
	void Start () {
		this.myGridRespawner = GameObject.FindGameObjectWithTag ("GridRespawner").GetComponent<RespawnObj>();
	}
	
	// Update is called once per frame
	void Update () {
		if (GameObject.FindGameObjectWithTag ("GameNode").GetComponent<Game> ().isRunning) {
			this.time += Time.deltaTime;
			if (this.time > 1.5) {
				this.time = 0;
				Spawn ();
			}
		}
	}
	private void Spawn(){
		int index = 0;
		do {
			index = Mathf.FloorToInt (Random.value * (this.myGridRespawner.statusArray.Length - 1));
		} while(!this.myGridRespawner.IsEmpty (index));
		int ranodmNumber = Mathf.FloorToInt (Random.value * 250);
		if (ranodmNumber % 3 == 0) {
			items.Add (Instantiate (mark));
			items [items.Count-1].transform.position = this.myGridRespawner.GetPosition (index);
		} else {
			if (ranodmNumber % 3 == 1) {
				items.Add (Instantiate (spray));
				items [items.Count-1].transform.position = this.myGridRespawner.GetPosition (index);
			} else {
				items.Add (Instantiate (upgrade));
				items [items.Count-1].transform.position = this.myGridRespawner.GetPosition (index);
				
			}
		}
		this.myGridRespawner.ChangeStatus (index, 1);
	}
	public void spawnFirst(){
		int z = 0;
		while (z<7) {
			int index = Mathf.FloorToInt (Random.value * (this.myGridRespawner.statusArray.Length - 1));
			Debug.Log ("Index: " + index);
			Debug.Log ("grid: " + this.myGridRespawner.statusArray.Length);
			if (this.myGridRespawner.IsEmpty (index)) {
				if (z % 3 == 0) {
					items.Add (Instantiate (mark));
					items [z].transform.position = this.myGridRespawner.GetPosition (index);
				} else {
					if (z % 3 == 1) {
						items.Add (Instantiate (spray));
						items [z].transform.position = this.myGridRespawner.GetPosition (index);
					} else {
						items.Add (Instantiate (upgrade));
						items [z].transform.position = this.myGridRespawner.GetPosition (index);
						
					}
				}
				this.myGridRespawner.ChangeStatus(index,1);
				z++;
			}
		}
	}
}
