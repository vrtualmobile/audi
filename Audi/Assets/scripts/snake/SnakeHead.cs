﻿using UnityEngine;
using System;
using System.Collections;

public class SnakeHead : MonoBehaviour {

    public float moveTime = 0.5f;
    public bool freeMode = false;
    private float fTurnRate = 90.0f;  // 90 degrees of turning per second
    private float fSpeed = 1f;  // Units per second of movement;
    private float time = 0f;

    private bool gameOver = false;
    private int dir = 0;

    private bool alreadyTouching;
    private Vector2 firstTouch;
    private Vector3 direction;
    private Vector2 lastTouch;

    void Start()
    {
        this.firstTouch = Vector2.zero;
        this.lastTouch = Vector2.zero;
        this.alreadyTouching = false;
        Input.multiTouchEnabled = false;
        this.direction = new Vector3(1, 0, 0);
    }

    void Update()
    {
        if (!freeMode)
        {
            time += Time.deltaTime;
            if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
            {
                if (dir == 2 || dir == 3)
                {
                    if (Input.GetKeyDown(KeyCode.LeftArrow))
                    {
                        transform.Rotate(-Vector3.forward * fTurnRate);

                        if (dir == 0)
                        {
                            dir = 3;
                        }
                        else
                        {
                            dir--;
                        }

                    }

                    if (Input.GetKeyDown(KeyCode.RightArrow))
                    {
                        transform.Rotate(Vector3.forward * fTurnRate);

                        if (dir == 3)
                        {
                            dir = 0;
                        }
                        else
                        {
                            dir++;
                        }
                    }
                }
                else
                {
                    if (Input.GetKeyDown(KeyCode.LeftArrow))
                    {
                        transform.Rotate(Vector3.forward * fTurnRate);
                        if (dir == 3)
                        {
                            dir = 0;
                        }
                        else
                        {
                            dir++;
                        }
                    }

                    if (Input.GetKeyDown(KeyCode.RightArrow))
                    {
                        transform.Rotate(-Vector3.forward * fTurnRate);
                        if (dir == 0)
                        {
                            dir = 3;
                        }
                        else
                        {
                            dir--;
                        }
                    }
                }

               // Debug.Log(transform.right);
            }

            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.touches.Length > 0)
                {
                    Vector2 currentTouch = Input.GetTouch(0).position;
                    if (this.alreadyTouching)
                    {
                        this.lastTouch = currentTouch;
                    }
                    else
                    {
                        this.alreadyTouching = true;
                        this.firstTouch = currentTouch;
                    }

                }
                else
                {
                    if (this.alreadyTouching)
                    {
                        float dist = Vector2.Distance(this.lastTouch, this.firstTouch);
                        float X = (lastTouch.x - firstTouch.x);
                        //float Y = Math.Abs(temp.y);

                        if (dir == 2 || dir == 3)
                        {
                            if (X < 0)
                            {
                                transform.Rotate(-Vector3.forward * fTurnRate);
                                if (dir == 0)
                                {
                                    dir = 3;
                                }
                                else
                                {
                                    dir--;
                                }
                            }
                            else
                            {
                                transform.Rotate(Vector3.forward * fTurnRate);
                                if (dir == 3)
                                {
                                    dir = 0;
                                }
                                else
                                {
                                    dir++;
                                }
                            }
                        }
                        else
                        {
                            if (X < 0)
                            {
                                transform.Rotate(Vector3.forward * fTurnRate);
                                if (dir == 3)
                                {
                                    dir = 0;
                                }
                                else
                                {
                                    dir++;
                                }
                            }
                            else
                            {
                                transform.Rotate(-Vector3.forward * fTurnRate);
                                if (dir == 0)
                                {
                                    dir = 3;
                                }
                                else
                                {
                                    dir--;
                                }
                            }
                        }
                        this.alreadyTouching = false;
                    }
                }
            }

            if (time > moveTime)
            {
                time = 0f;
                transform.localPosition = transform.localPosition + transform.right * fSpeed;
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Rotate(-Vector3.forward * fTurnRate * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Rotate(Vector3.forward * fTurnRate * Time.deltaTime);
            }
            transform.localPosition = transform.localPosition + transform.right * fSpeed * Time.deltaTime;
        }
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Przeszkoda")
        {
            Debug.Log("Game Over!");
            gameOver = true;
            Time.timeScale = 0;
        }
    }

    void OnGUI()
    {
        if(gameOver)
        {
            Camera.main.transform.FindChild("GameOver").gameObject.SetActive(true) ;
        }
    }
}
