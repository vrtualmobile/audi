﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;

public class Snake : MonoBehaviour {

	public GameObject snakeSegment;
	public GameObject gameState;
	public GameObject cloudPrefab;
	public GameObject colorMachine;
	public GameObject upgradeMachine;
	public GameObject endScreen;

	private GameObject snakeFront;
	private int speed = 1;
	private float moveFactor = 1;
	private Vector3 direction= new Vector3(1,0,0);
	private Vector3 oldDirection = new Vector3 (1, 0, 0);
	public List<Car> cars = new List<Car>();
	private float elapsedTime =0;
	private bool moveSnake = false;
	private float fSpeed = 1f;  // Units per second of movement;
	private float moveFrequency; //co ile sekund jest ruch
	// Use this for initialization
	void Start () {
	
		this.moveFrequency = 0.55f;
		snakeFront = GameObject.FindGameObjectWithTag ("Snake");

		//this.orders.Add(new CarOrder(40,Color.blue));


	}
	
	// Update is called once per frame
	void Update () {
		if (gameState.GetComponent<Game> ().isRunning) {

			if (this.moveSnake) {
				this.move ();
				this.moveSnake = false;
				this.elapsedTime = 0;
			} else {
				//Debug.Log("Czeka");
				this.elapsedTime += Time.deltaTime;
				if (this.elapsedTime > this.moveFrequency) {
					this.moveSnake = true;
				}
			}
		}
		//printCars ();
	}
	void OnCollisionEnter(Collision col){
		Debug.Log ("Collision Detected");
		if (col.gameObject.tag == "Upgrade") {
			Debug.Log ("Upgrade");
			if(this.cars.Count>0){
				gameState.GetComponent<Game> ().setRunning(false);
				Instantiate(this.upgradeMachine);
			}
			col.gameObject.GetComponent<Mark> ().collect ();
		} else {
			if (col.gameObject.tag == "Mark") {
				if(col.gameObject.transform.parent){
					if(!col.gameObject.GetComponent<Car>().isDelayed()){
						gameState.GetComponent<Game> ().setRunning(false);
						Debug.Log("Game Over!");
						processDeath();
					}
				}
				else{
					Debug.Log ("New Segment");
					col.gameObject.GetComponent<Mark> ().collect ();
					GameObject cloudTemp = (GameObject)(Instantiate(this.cloudPrefab));
					Vector3 parentPos = this.GetComponentInParent<Transform>().position; 
					cloudTemp.transform.position = parentPos+new Vector3(0.4f,0.5f,-0.3f);
					cloudTemp.GetComponent<Cloud>().setTime((this.cars.Count+1)*this.moveFrequency);

					GameObject temp = (GameObject)(Instantiate (snakeSegment));
					temp.transform.position = parentPos;
					temp.transform.Rotate(setSegmentRotation());
					temp.GetComponentInChildren<Car> ().setDelay(this.cars.Count+1);
					temp.GetComponentInChildren<Car>().setMove(this.oldDirection);
					this.cars.Add (temp.GetComponentInChildren<Car> ());
				}
			} else {
				if (col.gameObject.tag == "Spray") {
						if(this.cars.Count>0){
						Debug.Log ("Color");
						gameState.GetComponent<Game> ().setRunning(false);
						Instantiate(this.colorMachine);
					}
					col.gameObject.GetComponent<Mark> ().collect ();
				}
				else{
					if (col.gameObject.tag == "Przeszkoda")
					{
						gameState.GetComponent<Game> ().setRunning(false);
						Debug.Log("Game Over!");
						processDeath();
						//gameOver = true;
					}else{
						if(col.gameObject.tag == "GateBarrier"){
							if(this.oldDirection == new Vector3(0,0,1)){
								Debug.Log ("DIE");
								this.gameState.GetComponent<Game> ().setRunning (false);
								processDeath();
							}
						}else{
							if(col.gameObject.tag == "Gate"){
								Debug.Log("Oddanie");
								if(this.oldDirection == new Vector3(0,0,1)){
									Debug.Log ("DIE");
									this.gameState.GetComponent<Game> ().setRunning (false);
									processDeath();
								}else{
									
									List<CarOrder> orders = GameObject.FindGameObjectWithTag("OrderQueue").GetComponent<OrderQueue>().getOrders();
									if(orders.Count>0 && this.cars.Count>0){
										this.gameState.GetComponent<Game> ().setRunning (false);
										processOrders(orders);
										this.gameState.GetComponent<Game> ().setRunning (true);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void processDeath(){
		this.gameState.GetComponent<GameControl> ().Save ();
		Instantiate (this.endScreen);
	}
	private void printCars(){
		Debug.Log ("!!CARS!!");
		for (int l=0; l<this.cars.Count; l++) {
			this.cars[l].DebugCar(l);
		}
	}
	public void changeCarColor(string colorName){
		Debug.Log ("ColorChange");
		for (int z =0; z<this.cars.Count; z++) {
			if(!this.cars[z].getColored()){
				this.cars[z].changeColor(colorName);
				break;
			}
		}
		/*Debug.Log(this.GetComponent<MeshRenderer>().materials);
		this.GetComponent<MeshRenderer>().material.color=Color.green;*/
	}
	public void changeUpgrade(string upgradeName){
		Debug.Log ("UpgradeChange");
		for (int z = 0; z<this.cars.Count; z++) {
			if(!this.cars[z].hasUpgrade()){
				this.cars[z].setUpgrade(upgradeName);
				break;
			}
		}
		/*Debug.Log(this.GetComponent<MeshRenderer>().materials);
		this.GetComponent<MeshRenderer>().material.color=Color.green;*/
	}
	private void move(){
		Vector3 lastPos = this.oldDirection;
		//Vector3 lastPos = snakeFront.transform.position;

		changeDirection ();

		if (this.direction != this.oldDirection) {
			this.snakeFront.transform.Rotate(getRotation());
			this.oldDirection=this.direction;
		}

		this.snakeFront.transform.position += direction*fSpeed; //Time.deltaTime;
		for(int z =0; z<this.cars.Count;z++) {
			Vector3 storePos = this.cars[z].getMove();
			this.cars[z].move(lastPos);
			lastPos=storePos;
		}

	}
	private void processOrders(List<CarOrder> orders){
		Debug.Log ("Process orders");
		int k = 0;
		while(k<this.cars.Count){
			Debug.Log("Car "+k);
			if(cars[k].getColored()){
				for(int w=0;w<orders.Count;w++){
					if(orders[w].compare(this.cars[k])){
						removeCar(k);
					}
				}
				k++;
			}
			else{
				Debug.Log("increment");
				k++;
			}
		}
		clearOrders ();
		clearCars ();
	}
	private void clearCars(){

	}
	private void clearOrders(){
		GameObject.FindGameObjectWithTag ("OrderQueue").GetComponent<OrderQueue> ().clearOrders ();
	}
	private void removeCar(int index){
		Debug.Log ("Car Removed "+index);
		Vector3 removedPoistion = cars [index].getPosition ();
		Quaternion removedRotation = cars [index].getRotation ();
		Vector3 removedLastMove = cars [index].getMove ();
		Debug.Log (cars.Count);
		cars [index].sell ();
		cars.RemoveAt (index);
		Debug.Log (cars.Count);
		for (int z=index; z<cars.Count; z++) {
			Vector3 tempPos = cars[z].getPosition();
			Vector3 tempLastMove = cars[z].getMove();
			Quaternion tempRota = cars[z].getRotation();

			cars[z].setPosition (removedPoistion);
			cars[z].setMove (removedLastMove);
			cars[z].setRotation(removedRotation);

			removedPoistion=tempPos;
			removedRotation=tempRota;
			removedLastMove=tempLastMove;
		}
	}
	public void changeDirection(){
		Vector3 newDir = Vector3.forward;
		if (Application.platform == RuntimePlatform.Android) {
			newDir = this.GetComponent<androidInputHandler> ().getDirection ();
		} else {
			newDir = this.GetComponent<desktopInputHandler> ().getDirection ();
		}
		if (Vector3.Dot (newDir, direction) == 0) {
			direction = newDir * moveFactor;

		}
	}
	public void addCar(){
	}
	public void setMoveFactor(float newMoveFactor){
		moveFactor = newMoveFactor;
	}
	private Vector3 setSegmentRotation(){
		if (this.oldDirection == new Vector3 (1, 0, 0)) {
			return Vector3.zero;
		} else {
			if(this.oldDirection == new Vector3(-1,0,0)){
				return new Vector3(0,0,180);
			} else {
				if(this.oldDirection == new Vector3(0,0,1)){
					return new Vector3(0,0,-90);
				}else{
					return new Vector3(0,0,90);
				}
			}
		}
		return new Vector3(-1,0,0);
	}
	private Vector3 getRotation(){
		Vector3 result = Vector3.zero;
		if (this.oldDirection.x != 0) {
			if (this.oldDirection.x > 0) {
				if (this.direction.z < 0) {
					result.y = 90;
				} else {
					result.y = -90;
				}
			} else {
				if (this.direction.z < 0) {
					result.y = -90;
				} else {
					result.y = 90;
				}
			}
		} else {
			if (this.oldDirection.z > 0) {
				if (this.direction.x < 0) {
					result.y = -90;
				} else {
					result.y = 90;
				}
			} else {
				if (this.direction.x < 0) {
					result.y = 90;
				} else {
					result.y = -90;
				}
			}
		}
		return result;
	}
	void OnGUI(){

	}
}