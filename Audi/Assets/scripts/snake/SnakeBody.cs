﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SnakeBody : MonoBehaviour
{

    public GameObject goLeader = null;
    public float moveTime = 0.5f;
    private float time = 0f;
 
    void Update()
    {
        if (goLeader == null)
        {
            return;
        }

        time += Time.deltaTime;


        if (time > moveTime)
        {
            time = 0f;

            transform.position = goLeader.transform.position ;
            transform.rotation = goLeader.transform.rotation;
        }

    }
}
