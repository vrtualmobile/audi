using UnityEngine;
using System;


public class Mark : MonoBehaviour {

	private float timeLeft;
	private float rotationSpeed= 0.1f;
	private bool moveUp=true;
	Vector3 startPos;
	Vector3 bounceDir = Vector3.up;

	void Start(){
			
		timeLeft = UnityEngine.Random.value * 16;
		if (timeLeft < 4) {
			timeLeft=7;
		}
		startPos = this.transform.position;
		if (this.gameObject.tag == "Mark") {
			this.transform.Rotate(Vector3.up*UnityEngine.Random.value);
		}
	}
	void Update(){
		//if (GameObject.FindGameObjectWithTag("GameNode").GetComponent<Game> ().isRunning) {
		if (GameObject.FindGameObjectWithTag ("GameNode").GetComponent<Game> ().isRunning) {
			//this.bounce ();
			if (this.gameObject.tag != "Mark"){
				this.transform.Rotate(new Vector3(0,90*Time.deltaTime,0));
			}
			this.timeLeft-=Time.deltaTime;
			if(this.timeLeft<0){
				collect();
			}
			//transform.RotateAround (transform.GetComponent<Renderer> ().bounds.center, Vector3.up, rotationSpeed);
		}
		//}
	}
	private void bounce(){
		if (moveUp) {
			if (transform.position.y > startPos.y + 0.1){
				moveUp = false;
				bounceDir =Vector3.down;
			}else{
				transform.position+= bounceDir*Time.deltaTime*Time.deltaTime;

			}
		} else {
			if(transform.position.y<startPos.y){
				moveUp=true;
				bounceDir=Vector3.up;
			}else{
				transform.position+=bounceDir/2*Time.deltaTime;
			}
		}
	}
	public void collect(){
		GameObject.Destroy (this.gameObject);
	}
}