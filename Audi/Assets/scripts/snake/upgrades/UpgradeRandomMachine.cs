﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class UpgradeRandomMachine : MonoBehaviour
{

    public float next = 0.1f;

    private bool alreadyTouching;
    private Vector2 firstTouch;
    private Vector2 direction;
    private Vector2 lastTouch;

    private int currentImage = 0;
    private List<Image> images;
    private float time = 0f;
    private bool stop = false;

    // Use this for initialization
    void Start()
    {
        this.firstTouch = Vector2.zero;
        this.lastTouch = Vector2.zero;
        this.alreadyTouching = false;
        Input.multiTouchEnabled = false;
        this.direction = Vector2.zero;
        images = new List<Image>();

		Image[] temp = gameObject.GetComponentsInChildren<Image> ();
		for(int l=0; l < temp.Length;l++)
        {
            //Debug.Log(child.tag + " " + child.name);
            if (temp[l].tag == "Image")
            {
                images.Add(temp[l]);
            }
        }

        /*int temp = currentImage;
        foreach (Image image in images)
        {
            if (temp >= sprites.Count)
            {
                temp = 0;
            }
            image.sprite = sprites[temp++];
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.touches.Length > 0)
            {
				stop = true;
				triggerFunction();
			}
		}

        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                stop = true;
				triggerFunction();
            }
        }


        if (!stop)
        {
            time += Time.deltaTime;
            if (time > next)
			{
				Debug.Log ("Switch");
                time = 0f;
                /*if (currentImage >= images.Count)
                {
                    currentImage = 0;
                }
                else
                {
                    currentImage++;
                }
                int temp = currentImage;*/

				Sprite tempImg = images[0].GetComponentsInChildren<Image>()[1].sprite;

				for(int z=0;z<images.Count;z++){
					if(z<images.Count-1){
						images[z].GetComponentsInChildren<Image>()[1].sprite = images[z++].GetComponentsInChildren<Image>()[1].sprite;
					}
					else{
						images[z].GetComponentsInChildren<Image>()[1].sprite=tempImg;
					}
				}
                /*foreach (Image image in images)
                {
					if (temp >= images.Count)
                    {
                        temp = 0;
                    }
                    image = images[temp++];

                }*/
			}
        }

    }
	private void triggerFunction(){
		GameObject.FindGameObjectWithTag ("Snake").GetComponentInChildren<Snake> ().changeUpgrade (images[currentImage].GetComponentsInChildren<Image>()[1].name);
		GameObject.Destroy (this.gameObject);
	}
}
