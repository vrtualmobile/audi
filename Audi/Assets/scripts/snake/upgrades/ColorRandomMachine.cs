using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class ColorRandomMachine : MonoBehaviour
{
	public List<Sprite> sprites;
	public float next = 0.1f;

	private List<Color> colors = new List<Color> ();
	private bool alreadyTouching;
	private Vector2 firstTouch;
	private Vector2 direction;
	private Vector2 lastTouch;
	
	private int currentImage = 0;
	private List<Image> images;
	private float time = 0f;
	private bool stop = false;
	
	// Use this for initialization
	void Start()
	{
		this.firstTouch = Vector2.zero;
		this.lastTouch = Vector2.zero;
		this.alreadyTouching = false;
		Input.multiTouchEnabled = false;
		this.direction = Vector2.zero;
		images = new List<Image>();
		
		foreach (Image child in gameObject.GetComponentsInChildren<Image>())
		{
			//Debug.Log(child.tag + " " + child.name);
			if (child.tag == "Image")
			{
				images.Add(child);
			}
		}
		
		int temp = currentImage;
		foreach (Image image in images)
		{
			if (temp >= sprites.Count)
			{
				temp = 0;
			}
			image.sprite = sprites[temp++];
		}
	}
	
	// Update is called once per frame
	void Update()
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			if (Input.touches.Length > 0)
			{
				stop = true;
				triggerFunction();
			}
		}
		
		if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				stop = true;
				triggerFunction();
			}
		}
		
		if (!stop)
		{
			time += Time.deltaTime;
			if (time > next)
			{
				time = 0f;
				if (currentImage >= sprites.Count)
				{
					currentImage = 0;
				}
				else
				{
					currentImage++;
				}
				int temp = currentImage;
				foreach (Image image in images)
				{
					if (temp >= sprites.Count)
					{
						temp = 0;
					}
					image.sprite = sprites[temp++];
					
				}
			}
		}
		
	}
	private void triggerFunction(){
		GameObject.FindGameObjectWithTag ("Snake").GetComponentInChildren<Snake> ().changeCarColor (sprites[currentImage].name);
		GameObject.Destroy (this.gameObject);
	}
}
