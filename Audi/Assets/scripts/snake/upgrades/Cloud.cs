﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour {

	private float timeLeft;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.timeLeft -= Time.deltaTime;

		if(this.timeLeft < 0){
			GameObject.Destroy(this.gameObject);
		}
	}
	public void setTime(float time){
		this.timeLeft = time;
	}
}
