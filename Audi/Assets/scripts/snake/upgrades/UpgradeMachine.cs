﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UpgradeMachine : MonoBehaviour {


	private bool stop = false;
	private GameObject arrow;
	public float next = 0.1f;
	private float time = 0f;
	private RectTransform rt;
	private int move = 0;
	private bool selected = false;
	private float endtime = 0;
	private List<Image> images;
	private Image arrowImage;
	// Use this for initialization
	void Start () {
		arrow = GameObject.FindGameObjectWithTag ("Arrow");
		rt = arrow.GetComponent<RectTransform>();
		images = new List<Image>();

		Image[] temp = gameObject.GetComponentsInChildren<Image> ();
		for(int l=0; l < temp.Length;l++)
		{
			//Debug.Log(child.tag + " " + child.name);
			if (temp[l].tag == "Image")
			{
				images.Add(temp[l]);
			}
			if(temp[l].tag == "Arrow")
			{
				this.arrowImage=temp[l];
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Application.platform == RuntimePlatform.Android)
		{
			if (Input.touches.Length > 0)
			{
				stop = true;
				selected =true;
				time=0f;
				triggerFunction();
			}
		}
		
		if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				stop = true;
				selected =true;
				time=0f;
				triggerFunction();
			}
		}

		if (!stop) {
			time += Time.deltaTime;
			if (time > next) {
				time = 0f;
				if (move == 3) {
					move = 0;
					rt.position += new Vector3 (-420f, 0f, 0f);
				} else {
					rt.position += new Vector3 (140f, 0f, 0f);
					move++;
				}
			}
		}else {
			time+=Time.deltaTime;
			endtime+=Time.deltaTime;
			if(time<1.4f){
				if(endtime>0.2f){
					arrowImage.gameObject.SetActive(!selected);
					selected=!selected;
					endtime=0;
				}
			}
			else{
				destroyMyself();
			}
		}
	}
	private void triggerFunction(){
		GameObject.FindGameObjectWithTag ("Snake").GetComponentInChildren<Snake> ().changeUpgrade (images[move].GetComponentsInChildren<Image>()[1].sprite.name);
	}
	private void destroyMyself(){
		GameObject.Destroy (this.gameObject);
		GameObject.FindGameObjectWithTag("GameNode").GetComponent<Game> ().setRunning (true);
	}
}
