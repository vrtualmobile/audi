using System;
using UnityEngine;

public class CarOrder: MonoBehaviour
{
	private Color myColor = Color.yellow;
	private float orderTime=25;
	private	string upgrade="";
	private bool completed = false;
	private bool toErase = false;
	void Start(){

	}

	void Update(){
		if (this.toErase) {
			eraseOrder();
		}
		if (this.orderTime < 0) {
			orderExpired();
		} else {
			this.orderTime-=Time.deltaTime;
		}
	}
	public void setOrderColor(Color newColor){
		this.myColor = newColor;
	}
	public void setTimeLeft(int newTime){
		this.orderTime = newTime;
	}
	public void setOrderUpgrade(string upgradeName){
		this.upgrade = upgradeName;
	}
	public bool compare(Car target){
		if (!this.completed) {
			Debug.Log("COMAPRING: "+target.getUpgrade()+"-"+this.upgrade+"  and  "+target.getCarColor()+"-"+this.myColor); 
			if(target.getUpgrade() == this.upgrade && this.myColor == target.getCarColor()){
				Debug.Log("ORDER COMPLETED");
				this.completed = true;
				return true;
			}
		} else {
			return false;
		}
		return false;
	}
	private void orderExpired(){

	}
	public void setErase(bool newState){
		this.toErase = newState;
	}
	public void eraseOrder(){
		GameObject.Destroy (this.gameObject);
	}
	public void completeOrder(){
		this.completed = true;
	}
	public bool isCompleted(){
		return this.completed;
	}
}

