﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOptions : MonoBehaviour {

	public GameObject parent;

	private GameObject mainMenu;
	private Button survival;
	private Button sandbox;
	// Use this for initialization
	void Start () {
		processButtons (this.parent.GetComponentsInChildren <Button>());
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
		{
			this.mainMenu.SetActive(true);
			this.parent.SetActive(false);
		}
	}
	private void startSurvival(){
		GameObject.FindGameObjectWithTag ("GameNode").GetComponent<Game> ().setRunning (true);
		this.parent.SetActive (false);

	}
	private void startNormal(){
		GameObject.FindGameObjectWithTag ("GameNode").GetComponent<Game> ().setRunning (true);
		this.parent.SetActive (false);
	}
	private void processButtons(Button[] buttons){
		foreach (Button bttn in buttons) {
			if (bttn.name == "Survival") {
				this.survival = bttn;
				this.survival.onClick.AddListener (this.startSurvival);
			}
			if (bttn.name == "Normal") {
				this.sandbox = bttn;
				this.sandbox.onClick.AddListener (this.startNormal);
			}
		}
	}
	public void setMainMenu(GameObject menu){
		this.mainMenu=menu;
	}
}
