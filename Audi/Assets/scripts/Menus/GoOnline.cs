﻿using UnityEngine;
using System.Collections;

public class GoOnline : MonoBehaviour {

	public GameObject parent;

	private GameObject mainMenu;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
		{
			this.mainMenu.SetActive(true);
			this.parent.SetActive(false);
		}
	}
	public void setMainMenu(GameObject menu){
		this.mainMenu=menu;
	}
}
