﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndScreenMenu : MonoBehaviour {

    private Button online;
    private Button back;

    void Start()
    {
        processButtons(this.GetComponentsInChildren<Button>());
    }

    private void goOnline()
    {
        Application.OpenURL("https://random-puli.rhcloud.com/login");
    }
    private void backToMenu()
    {
		
		GameObject.FindGameObjectWithTag ("GameNode").GetComponent<Game> ().restarted=true;
		Application.LoadLevel (Application.loadedLevel - 1);
    }
	private void Restart(){
		GameObject.FindGameObjectWithTag ("GameNode").GetComponent<Game> ().restarted=true;
		Application.LoadLevel (Application.loadedLevel);
	}
    private void processButtons(Button[] buttons)
    {
        foreach (Button bttn in buttons)
        {
            if (bttn.tag == "ToFB")
            {
                
                bttn.onClick.AddListener(this.goOnline);
            }
            if (bttn.tag == "ToMenu")
            {
				bttn.onClick.AddListener(this.backToMenu);

			}
			if (bttn.tag == "ToRestart")
			{
				bttn.onClick.AddListener(this.Restart);
				
			}
        }
    }
}
