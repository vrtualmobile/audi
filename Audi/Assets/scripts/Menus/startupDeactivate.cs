﻿using UnityEngine;
using System.Collections;

public class startupDeactivate : MonoBehaviour {

	public GameObject gameState;

	private GameObject[] list;
	// Use this for initialization
	void Start () {
		list = GameObject.FindGameObjectsWithTag ("StartDeactivated");
		for (int i=0; i<this.list.Length; i++) {
			list[i].SetActive(false);
		}
	}

	void Update(){
		if (this.gameState.GetComponent<Game> ().isRunning) {
			activate();
		}

	}
	private void activate(){
		for (int i=0; i<this.list.Length; i++) {
			list [i].SetActive (true);
		}
	}
}
