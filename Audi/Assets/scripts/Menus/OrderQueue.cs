﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class OrderQueue : MonoBehaviour {

	public GameObject order;
	public List<Sprite> clients = new List<Sprite> ();
	public List<Sprite> spriteColors = new List<Sprite> ();
	public List<Sprite> spriteUpgrade = new List<Sprite> ();

	private List<string> upgrades = new List<string> ();
	private List<Color> colors = new List<Color> ();

	private List<CarOrder> orders = new List<CarOrder> ();
	private float OrderDelay =0;
	// Use this for initialization
	void Start () {
		this.upgrades.Add ("98");
		this.upgrades.Add ("on");
		this.upgrades.Add ("quatro");
		this.upgrades.Add ("rs");

		this.colors.Add (Color.yellow);
		this.colors.Add (Color.red);
		this.colors.Add (Color.blue);
		this.colors.Add (Color.black);

		RectTransform rt = this.GetComponent<RectTransform>();
		rt.sizeDelta = new Vector2 (Screen.width, Screen.height);
		RandomOrder ();
	}
	
	// Update is called once per frame
	void Update () {
		if (GameObject.FindGameObjectWithTag("GameNode").GetComponent<Game> ().isRunning) {
			this.OrderDelay += Time.deltaTime;
			Debug.Log (this.orders.Count);
			if (OrderDelay > 11) {
				if (this.orders.Count < 5) {
					RandomOrder ();
				}
				OrderDelay = 0;
			}
		}
	}
	private void RandomOrder(){
		if (Random.value > 0.75) {
			createOrder (this.clients [Mathf.FloorToInt (Random.value *(this.clients.Count-1))],
			             this.upgrades [Mathf.FloorToInt (Random.value * (this.upgrades.Count-1))],
			             this.colors [Mathf.FloorToInt (Random.value *( this.colors.Count-1))]);
		} else {
			createOrder (this.clients [Mathf.FloorToInt (Random.value *(this.clients.Count-1))],
			             this.colors [Mathf.FloorToInt (Random.value * (this.colors.Count-1))]);
		}
	}
	private void createOrder(Sprite client, string upgradeName, Color orderColor){
		GameObject newOrder = Instantiate (order);
		newOrder.transform.SetParent(this.transform,false);
		newOrder.transform.position+= new Vector3(this.orders.Count*85,0,0);
		newOrder.GetComponent<CarOrder> ().setOrderColor(orderColor);
		newOrder.GetComponent<CarOrder> ().setOrderUpgrade(upgradeName);
		foreach (Image temp2 in newOrder.GetComponentsInChildren<Image> ()) {
			if(temp2.tag=="ClientPart"){
				temp2.sprite=client;
			}
			if(temp2.tag=="ColorPart"){
				if(orderColor==Color.black){
					temp2.sprite=this.spriteColors[0];
				}if(orderColor==Color.yellow){
					temp2.sprite=this.spriteColors[3];
				}if(orderColor==Color.red){
					temp2.sprite=this.spriteColors[1];
				}if(orderColor==Color.blue){
					temp2.sprite=this.spriteColors[2];
				}
			}
			if(temp2.tag=="OrderPart"){
				if(upgradeName==this.upgrades[0]){
					temp2.sprite=this.spriteUpgrade[0];
				}if(upgradeName==this.upgrades[1]){
					temp2.sprite=this.spriteUpgrade[1];
				}if(upgradeName==this.upgrades[2]){
					temp2.sprite=this.spriteUpgrade[2];
				}if(upgradeName==this.upgrades[3]){
					temp2.sprite=this.spriteUpgrade[3];
				}
			}
		}
		this.orders.Add (newOrder.GetComponent<CarOrder> ());
	}
	private void createOrder(Sprite client, Color orderColor){
		GameObject newOrder = Instantiate (order);
		newOrder.transform.SetParent(this.transform,false);
		newOrder.transform.position+= new Vector3(this.orders.Count*85,0,0);
		newOrder.GetComponent<CarOrder> ().setOrderColor(orderColor);
		foreach (Image temp2 in newOrder.GetComponentsInChildren<Image> ()) {
			if(temp2.tag=="ClientPart"){
				temp2.sprite=client;
			}
			if(temp2.tag=="ColorPart"){
				if(orderColor==Color.black){
					temp2.sprite=this.spriteColors[0];
				}if(orderColor==Color.yellow){
					temp2.sprite=this.spriteColors[3];
				}if(orderColor==Color.red){
					temp2.sprite=this.spriteColors[1];
				}if(orderColor==Color.blue){
					temp2.sprite=this.spriteColors[2];
				}
			}
			if(temp2.tag=="OrderPart"){
				temp2.gameObject.SetActive(false);
			}
		}

		this.orders.Add (newOrder.GetComponent<CarOrder> ());
	}
	private void addOrder(CarOrder newOrder){
		this.orders.Add(newOrder);
	}
	public List<CarOrder> getOrders(){
		return this.orders;
	}
	public void reDrawOrders(){
		Vector3 posTemp = this.orders[0].transform.position;
		for(int ol=0;ol<this.orders.Count;ol++){
			this.orders[ol].transform.position=new Vector3(ol*85,posTemp.y,0);
		}
	}
	public void clearOrders(){
		int b = 0;
		while(b < orders.Count){
			if(this.orders[b].isCompleted()){
				Debug.Log("ADD POINT");	
				this.orders[b].setErase(true);
				this.orders.Remove(orders[b]);
				GameObject.FindGameObjectWithTag("GameNode").GetComponent<GameControl> ().addPoint ();
			}
			else{
				b++;
			}
		}
		if (orders.Count > 0) {
			reDrawOrders ();
		}
		Debug.Log ("END");
	}
}
