﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

using SimpleJSON;

public class Menu : MonoBehaviour {

	public GameObject parent;
	public GameObject gameOptions;
	public GameObject gameState;

	private GameObject gameOption;
	private Button start;
	private Button store;
	private Button online;
	private Button exit;

	// Use this for initialization
	void Start () {
		FB.Init (OnFBInit, OnHideUnity, null); // krok 0: niezbędne
		processButtons (this.parent.GetComponentsInChildren <Button>());
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	#region Facebook

	private void OnFBInit () {
		return;
	}

	private void OnHideUnity (bool isGameShown) {
		return;
	}

	private void AuthCallback (FBResult result) { // krok 2: po uwierzytelnieniu
		Debug.Log ("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);

		FB.API ("/me/scores", Facebook.HttpMethod.GET, ScoresCallback); // krok 3: pobierz punkty
	}

	private void ScoresCallback (FBResult result) {

		var score = (JSON.Parse (result.Text))["data"][0]["score"].AsInt; // krok 4: parsuj

		var feed = new WWWForm ();
		feed.AddField ("message", "mam " + score + " punktów!");

		FB.API ("/me/feed", Facebook.HttpMethod.POST, PostFeedCallback, feed); // krok 5: wyślij na wall

		var scores = new WWWForm ();
		scores.AddField ("score", score + 1);
		
		FB.API ("/me/scores", Facebook.HttpMethod.POST, PostScoreCallback, scores); // krok 6: dodaj punkt
	}

	private void PostFeedCallback (FBResult result) {
		Debug.Log ("done!");
	}

	private void PostScoreCallback (FBResult result) {
		Debug.Log ("done!");
	}

	#endregion

	private void startGame(){
        Application.LoadLevel((Application.loadedLevel + 1));
		//this.gameOption = Instantiate (gameOptions);
		//this.gameOption.GetComponent<GameOptions> ().setMainMenu (this.parent);
		//this.parent.SetActive (false);

	}

	private void openStore(){

	}

	private void goOnline(){
		FB.Login ("publish_actions", AuthCallback); // krok 1: pokazanie ekranu logowania
	}

	private void quitApp(){
		Application.Quit();
	}
	private void processButtons(Button[] buttons){
		foreach (Button bttn in buttons) {
			if(bttn.name == "StartGame"){
				this.start=bttn;
				this.start.onClick.AddListener (this.startGame);
			}
			if(bttn.name == "Store"){
				this.store=bttn;
				this.store.onClick.AddListener (this.openStore);
			}
			if(bttn.name == "Online"){
				this.online=bttn;
				this.online.onClick.AddListener (this.goOnline);
			}
			if(bttn.name == "Exit"){
				this.exit=bttn;
				this.exit.onClick.AddListener (this.quitApp);
			}
		}
	}

	public void setActive(bool newState){
		this.parent.SetActive(newState);
	}
}
