﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class CameraMove : MonoBehaviour {

	public Transform target;            // The position that that camera will be following.
	public float smoothing = 5f;        // The speed with which the camera will be following.
	Vector3 offset;                     // The initial offset from the target.

	private GameObject mainCamera;
	private Grid grid;

	private GameObject snake;
	private Vector3 snakeLastPos;
	// Use this for initialization
	void Start () {

		mainCamera = GameObject.Find("MainCamera");
		grid = new Grid (30);
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnPostRender(){
		grid.Draw();
	}
}
