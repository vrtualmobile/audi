﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenFader : MonoBehaviour {

    public float fadeSpeed = 30f;
    private Image img;
    public bool toBlack = false;
    public bool toClear = false;

	// Use this for initialization
	void Start () {
        img = GetComponent<Image>();
		RectTransform rt = this.GetComponent<RectTransform>();
		rt.sizeDelta = new Vector2 (Screen.width, Screen.height);
	}
	
	// Update is called once per frame
	void Update () {
        if (toBlack)
        {
            FadeToBlack();
            toBlack = false;
        }

        if(toClear)
        {
            FadeToClear();
            toClear = false;
        }
	
	}

    void FadeToClear()
    {
        img.CrossFadeAlpha(0f, .5f, false);
    }
 
 
    void FadeToBlack()
    {
        img.CrossFadeAlpha(1f, .5f, false);
        
    }

}
