﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System.Collections.Generic;

public class Game : MonoBehaviour {


	public GameObject WallBlock;
	public GameObject Mark;
	public GameObject mainCamera;

	public bool restarted=false;
	public bool isRunning = true;
	public int numberOfColors = 2;
	public List<Color> availableColors = new List<Color>();
	private GameObject mainMenu;

	private int platformSize = 10;
	private int respawnTime = 10;
	private int timeTorespawn = 0;
	private Vector3 scale = Vector3.one;

	//bolleans for menu operations
	private bool choosingGameMode;

	// Use this for initialization
	void Start () {
		//this.isRunning = false;
		this.isRunning = true;
		this.choosingGameMode = false;

		mainMenu = GameObject.FindGameObjectWithTag ("Menu");
		mainCamera = GameObject.FindGameObjectWithTag ("MainCamera");

		this.availableColors.Add (Color.blue);
		this.availableColors.Add (Color.green);
		this.availableColors.Add (Color.yellow);
		this.availableColors.Add (Color.red);
		this.availableColors.Add (Color.black);
		/*for (int i = 0; i < platformSize; i++) {
			Instantiate(WallBlock, new Vector3(i-platformSize/2, 0, -platformSize), Quaternion.identity);
			Instantiate(WallBlock, new Vector3(i-platformSize/2, 0, platformSize), Quaternion.identity);
		}
		for (int i = 0; i < platformSize*2-1; i++) {
			Instantiate(WallBlock, new Vector3(-platformSize/2, 0, -platformSize+1+i)	, Quaternion.identity);
			Instantiate(WallBlock, new Vector3(platformSize-1-platformSize/2, 0, -platformSize+1+i), Quaternion.identity);
		}
		Instantiate (Mark, new Vector3 (Random.Range (-platformSize / 2 + 1, platformSize / 2 - 1), 0, Random.Range (-platformSize + 1, platformSize - 1)), Quaternion.identity);
		Instantiate (Mark, new Vector3 (Random.Range (-platformSize / 2 + 1, platformSize / 2 - 1), 0, Random.Range (-platformSize + 1, platformSize - 1)), Quaternion.identity);
		*/
	}
	
	// Update is called once per frame
	void Update () {
		if (restarted) {
			restarted=false;
			this.isRunning=true;
		}
		if (Input.GetKey(KeyCode.Escape))
		{
			this.GetComponent<GameControl>().Save();
			Application.Quit();
		}
	}

	public void setRunning(bool newState){
		this.isRunning = newState;
	}

}


