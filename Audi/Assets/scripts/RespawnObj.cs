﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RespawnObj : MonoBehaviour {

    public int[] statusArray;
    public float playerCheck = 0.5f;
    public GameObject player;

    public List<Transform> respawnFields;
    private float time = 0f;

	// Use this for initialization
	void Start () {
        respawnFields = new List<Transform>(GetComponentsInChildren<Transform>());
        statusArray = new int[respawnFields.Count];

        for(int i = 0; i < statusArray.Length; i++)
        {
            statusArray[i] = 0;
        }
		GameObject.FindGameObjectWithTag ("GameNode").GetComponent<ItemRespawner> ().spawnFirst ();
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;

        if (time > playerCheck)
        {
            time = 0;

            for (int i = 0; i < statusArray.Length; i++)
            {
                if (statusArray[i] == 2)
                {
                    statusArray[i] = 0;
                }
            }

            statusArray[respawnFields.IndexOf(FindClosestRespawn(player.transform.position).transform)] = 2;

            Snake temp = player.GetComponent<Snake>();
            foreach (Car c in temp.cars)
            {
                statusArray[respawnFields.IndexOf(FindClosestRespawn(c.transform.position).transform)] = 2;
            }
        }
	}

    private GameObject FindClosestRespawn(Vector3 pos)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("RespawnObj");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - pos;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

    public void ChangeStatus(int index, int value)
    {
        statusArray[index] = value;
    }

    public bool IsEmpty(int index)
    {
        if (statusArray[index] != 1 && statusArray[index] != 2)
        {
            return true;
        }

        return false;
    }

    public Vector3 GetPosition(int index)
    {
        return respawnFields[index].transform.position+new Vector3(0.4f,0,0.6f);
    }

}
