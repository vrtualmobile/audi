﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

    public static GameControl control;

    public float points;
	//public TimeSpan time;
	//public int currentLevel;

	//public bool timeFlag;
	void Start(){
		Load ();
		GameObject.FindGameObjectWithTag ("CoinMenu").GetComponentInChildren<Text> ().text = points.ToString();
	}
	/*void Awake () {
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else if(control != this)
        {
            Destroy(gameObject);
        }
	}*/
	
	// Update is called once per frame
	void Update () {
        //if (timeFlag) {
        //    time = time + TimeSpan.FromSeconds(Time.deltaTime);
        //}
	}

    //void OnGUI()
    //{
        //GUI.Label(new Rect(10, 10, 150, 30), "Points: " + points);
        //GUI.Label(new Rect(10, 40, 150, 30), "Time: " + time);
        //GUI.Label(new Rect(10, 70, 150, 30), "Level: " + currentLevel);
		//GUI.Label(new Rect(10, 70, 150, 30), "Level: " + (Application.loadedLevel + 1));
    //}

    //void OnDisable()
    //{
    //    Save();
    //}

    //void OnEnable()
    //{
    //    Load();
    //}
	public float getPoints(){
		return points;
	}

	public void addPoint(){
		points += 1;
		GameObject.FindGameObjectWithTag ("CoinMenu").GetComponentInChildren<Text> ().text = points.ToString();
	}
    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");
        PlayerData data = new PlayerData();
        data.points = points;
        //data.time = time;
		//data.currentLevel = currentLevel;
		//data.currentLevel = (Application.loadedLevel + 1);

        bf.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize (file);
			file.Close ();

			points = data.points;
			//time = data.time;
			//currentLevel = data.currentLevel;
			//Application.LoadLevel(Application.loadedLevel);
		} else {
			points=0;
			Save();
		}
    }
}
