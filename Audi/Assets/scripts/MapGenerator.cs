﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class MapGenerator : MonoBehaviour
{

    public List<GameObject> modules;
    public String path;
    public float level;

    // Use this for initialization
    void Start()
    {
        if (modules.Count > 0)
        {

            string fileContent = File.ReadAllText(Application.dataPath + path);
            string[] integerStrings = fileContent.Split(new char[] { ' ', '\t', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            int[] integers = new int[integerStrings.Length];
            for (int n = 0; n < integerStrings.Length; n++)
            {
                integers[n] = int.Parse(integerStrings[n]);
            }


            //foreach (int i in integers)
            //{
            //    Debug.Log(i);
            //}

            int x = 0;
            int z = 0;
            for (int i = 0; i < integers.Length; i++)
            {
                if (integers[i] == -9)
                {
                    x = 0;
                    z--;
                    continue;
                }
                else if (integers[i] == -1)
                {
                    x++;
                    continue;
                }
                if (modules[integers[i]] != null)
                {
                    Instantiate(modules[integers[i]], new Vector3(0.01f * x, level, 0.01f * z), modules[integers[i]].transform.rotation);
                }
                x++;
    
            }

            //if (File.Exists(Application.dataPath + path))
            //{
            //    StreamReader sr = new StreamReader(Application.dataPath + path);
            //    float i = 0;
            //    while (sr.Peek() >= 0)
            //    {
            //        i += 0.01f;
            //        if (Char.IsDigit((char)sr.Read()))
            //        {

            //        }
            //        Debug.Log((char)sr.Read());
            //    }
            //}
            //else
            //{
            //    Debug.LogError("File: " + Application.dataPath + path + " doesen't exist!");
            //}

            //while(

            //for (float i = 0; i < 1; i += 0.01f)
            //{
            //    for (float j = 0; j < 1; j += 0.01f)
            //    {
            //        Instantiate(modules[0], new Vector3(i, 0f, j), Quaternion.identity);
            //    }
            //}
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
